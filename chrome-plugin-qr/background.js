﻿var resdata ={};
//获取URL
function checkForValidUrl(tabId, changeInfo, tab) {
	//获取上下文URL
	resdata.url=tab.url;
	//显示按钮
	chrome.pageAction.show(tabId);
};
//添加事件监听
chrome.tabs.onUpdated.addListener(checkForValidUrl);

//获取切换tab的事件
function checkTabChange(activeInfo){
	chrome.tabs.get(activeInfo.tabId, function(tab){
	//获取上下文URL
	resdata.url=tab.url;
	})
}
//获取切换tab的事件
chrome.tabs.onActivated.addListener(checkTabChange);