//进行Google的二维码地址生成，并把浏览器地址进行一个转码，一定要转码
//chs=宽高值x宽高值&cht=qr&chld=等级|边距&chl=内容
//宽高值：生成二维码尺寸，单位是像素，目前生成的二维码都是正方形的，所以两个宽高值都设置为一样的值
//等级：四个等级，L-默认：可以识别已损失的7%的数据；M-可以识别已损失15%的数据；Q-可以识别已损失25%的数据；H-可以识别已损失30%的数据
//边距：生成的二维码离图片边框的距离
//内容：生成二维码的内容，但一定要URLENCODE
function getQrcodeUrl(data, size, level, margin) {
    size = size || 320;
    level = level || 'L';
    margin = margin | 0;
    return [
        'https://chart.googleapis.com/chart?cht=qr&choe=UTF-8&',
        '&chs='+size+'x'+size,
        '&chld='+level+'|'+margin,
        '&chl='+encodeURIComponent(data)
    ].join('');
}

//页面加载完毕后，进行执行该事件
document.addEventListener('DOMContentLoaded', function () {
	//获取后台js执行的数据
	var data = chrome.extension.getBackgroundPage().resdata;
	//var gapis="https://chart.googleapis.com/chart?cht=qr&chs=200x200&choe=UTF-8&chld=L|4&chl=";
	document.getElementById("qrCode").src=getQrcodeUrl(data.url);
});

