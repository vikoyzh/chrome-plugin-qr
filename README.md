#chrome-plugin-qr
项目名称：浏览器地址二维码生成器
描述：
通过Google插件的方式，嵌入了地址栏中，对浏览器地址栏的地址进行一个二维码生成，主要方便于手机访问电脑网页，无需在手机浏览器中输入地址

使用方式
开启Google的开发者模式，然后加载正在开发的项目即可使用


注意：
由于该项目是调用Google的一个JS二维码生成插件，所以中国区域有可能需要开启VPN才能访问

